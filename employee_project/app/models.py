from email.policy import default
from django.db import models

# Create your models here.


class Employees(models.Model):
    emp_name = models.CharField(max_length=20)


class Details(models.Model):
    employee = models.ForeignKey(Employees, on_delete=models.CASCADE)
    inputdate1 = models.CharField(max_length=20)
    inputdate2 = models.CharField(max_length=20)
    reference = models.CharField(max_length=20)
    conracts = models.CharField(max_length=20)
    structure = models.CharField(max_length=20)
    payslip_name = models.CharField(max_length=20)
    credit_note = models.BooleanField(default=False, blank=True, null=True)
    company = models.CharField(max_length=20)
    status = models.CharField(max_length=20)
    is_deleted = models.BooleanField(default=False)
