# Generated by Django 3.2.15 on 2022-10-14 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0002_alter_details_credit_note"),
    ]

    operations = [
        migrations.AddField(
            model_name="details",
            name="company",
            field=models.CharField(default=1, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="details",
            name="status",
            field=models.CharField(default=1, max_length=20),
            preserve_default=False,
        ),
    ]
