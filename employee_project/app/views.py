from http.client import HTTPResponse
from django.shortcuts import render,redirect
from .models import *
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import JsonResponse

from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.core import serializers
# from weasyprint import HTML
import pandas as pd
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
import openpyxl
import sweetify
from django.template.loader import get_template
from xhtml2pdf import pisa
import xlwt
from django.http import HttpResponse

import re 

# Create your views here.


def view_page(request):
    requested_html = re.search(r'^text/html', request.META.get('HTTP_ACCEPT'))
    if not requested_html:
        key = request.GET.get("key")
        if key == 'null':
            obj =Details.objects.select_related('employee').filter(is_deleted=False)
        else:
            obj =Details.objects.select_related('employee').filter(is_deleted=False,payslip_name__icontains=key)
        # obj =Details.objects.select_related('employee').filter(is_deleted=False)
        eobj=Employees.objects.all()
        obj_pg = Paginator(obj, per_page=3)
        page_number = request.GET.get("page")
        dataObj = obj_pg.page(page_number)
        has_next=dataObj.next_page_number() if dataObj.has_next() else None
        has_prev=dataObj.previous_page_number() if dataObj.has_previous() else None
        page_range=obj_pg.num_pages
        qs_json=serializers.serialize("json",dataObj )
        qs_emp=serializers.serialize("json",eobj )
        data={"dataObj":qs_json,"has_next":has_next,"has_prev":has_prev,"page_range":page_range,"eobj":qs_emp}
        return JsonResponse(data, content_type='application/json')
    return render(request, "settings.html")


def pdf_page(request):
    ids=request.GET.getlist('selected_data')
    li = list(ids[0].split(","))
    cleanedList = [x for x in li if x != 'NaN']
    data_pdf = Details.objects.filter(id__in=cleanedList)
    template_path = 'pdf_templates.html'
    context = {'data_pdf': data_pdf}
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename="report.pdf"'
    template = get_template(template_path)
    html = template.render(context)
    pisa_status = pisa.CreatePDF(
        html, dest=response)
    if pisa_status.err:
        return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response

def create(request):
    if request.method == "POST":
        emp = request.POST.get("Employee")
        inputdate1 = request.POST.get("Inputdate1")
        inputdate2 = request.POST.get("Inputdate2")
        reference = request.POST.get("Reference")
        conracts = request.POST.get("Conracts")
        structure = request.POST.get("Structure")
        payslip_name = request.POST.get("Payslip_name")
        credit_note = request.POST.get("Credit_note")
        credit_obj = True if credit_note == "True" else False
        emp_obj = Employees.objects.get(id=emp)
        company = "Saudi Mohammed Jafar Al Hadad General Cont. Co."
        status = "WAITING"
        Details.objects.create(
            employee=emp_obj,
            inputdate1=inputdate1,
            inputdate2=inputdate2,
            reference=reference,
            conracts=conracts,
            structure=structure,
            payslip_name=payslip_name,
            credit_note=credit_obj,
            company=company,
            status=status,
        )
        return redirect('/view_page')
    return render(request, "settings.html")

def delete(request):
    requested_html = re.search(r'^text/html', request.META.get('HTTP_ACCEPT'))
    if not requested_html:
        data = request.GET.getlist("delete_data[]")
        # res = [eval(i) for i in data]
        li = list(data[0].split(","))
        cleanedList = [x for x in li if x != 'NaN']
        print(cleanedList,"............................fffff")
        Details.objects.filter(id__in=cleanedList).update(is_deleted=True)
        return JsonResponse({"status":"success"})

def export_data_to_excel(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="details.xls"'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Employee details Data') 

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['employee', 'date from ', 'date to', 'reference','contracts','structure' ,'payslip_name','credit_note','company','status']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style) 

    font_style = xlwt.XFStyle()
    exportid=request.GET.getlist('selected_data')
    li = list(exportid[0].split(","))
    cleanedList = [x for x in li if x != 'NaN']

    rows = Details.objects.filter(id__in=cleanedList).values_list('employee__emp_name', 'inputdate1', 'inputdate2', 'reference','conracts','structure','payslip_name','credit_note','company','status')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response

def import_data_to_db(request):
    if request.method == 'POST':
        excel_file = request.FILES["fileid"]
        wb = openpyxl.load_workbook(excel_file, data_only=True)
        worksheet = wb["Sheet1"]
        index = 0
        for row in worksheet.iter_rows():
            print(index)
            if (index == 0):
                index += 1
                continue
            else:
                emp_obj = Employees.objects.get(id=row[2].value)
                index += 1
                details = Details()
                details.employee = emp_obj
                details.inputdate1 = row[3].value
                details.inputdate2 = row[4].value
                details.reference = row[5].value
                details.conracts = row[6].value
                details.structure = row[7].value
                details.payslip_name = row[8].value
                details.credit_obj = request.user
                # company=company,
                # status=status,
                details.save()
                
                # sweetify.success(
                #     request, 'Success',
                #     icon="success",
                #     type="success",
                #     text='details Added Successfully',
                #     button='OK',
                #     timer=2000
                # )
    return redirect('/view_page/')

# @login_required
def login(request):
    from django.contrib import messages, auth
    
    if request.method == 'POST':
        print("llllllllllllllllllllllll")
        email = request.POST.get('email')
        print(email,"....................>>>>>>>>>>>")
        password = request.POST.get('psw')
        print(password,"....................>>>>>>>>>>>")
        user = auth.authenticate(username=email, password=password)
        print(user,"..............................>???")
        if user is not None :
            print("4")
            auth.login(request, user)
            return redirect('/view_page')
        else:
            print("5")
            messages.error(request, 'Invalid login credentials')
            return render(request, 'login.html', {'email': email, 'password': password})
    else:
        print("6")
        if request.user.is_authenticated:
            return redirect('/view_page')
        return render(request, 'login.html')

def sign_up(request):
    if request.method=='POST':
        email=request.POST.get('email')
        password=request.POST.get('psw')
        username=request.POST.get('username')
        User.objects.create_user(username=username,email=email,password=password,is_staff=True)
        return redirect('/view_page/')
    else:
        return render(request,'sign_up.html')


def check_all(request):
    data=Details.objects.filter(is_deleted=False)
    id_list=list(Details.objects.filter(is_deleted=False).values_list('pk',flat=True))

    return JsonResponse({"lst":id_list})




# def export_data_to_excel(request):
#     exportid=request.GET.getlist('selected_data')
#     li = list(exportid[0].split(","))
#     cleanedList = [x for x in li if x != 'NaN']
#     objs=Details.objects.filter(id__in=cleanedList).values()
#     pd.DataFrame(objs).to_excel('output.xlsx')
#     # response = HttpResponse(content_type='application/vnd.ms-excel')
#     # response['Content-Disposition'] = 'attachment; filename=output.xlsx'
#     # xlsx_data = WriteToExcel(weather_period, town)
#     # response.write(xlsx_data)
#     # return response
#     return JsonResponse({'status':200})
          
                    

