"""employee_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from app import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("import_to_db/", views.import_data_to_db, name="import_to_db"),
    path("create/", views.create, name="create"),
    path("delete/", views.delete, name="delete"),
    path("export_excel/", views.export_data_to_excel, name="export_excel"),
    path("pdf_page/", views.pdf_page, name="pdf_page"),
    path("view_page/", views.view_page, name="view_page"),
    path("sign_up/", views.sign_up, name="sign_up"),
    path("", views.login, name="login"),
    path("checkall/", views.check_all, name="checkall"),

    
    # path("export_users_xls/", views.export_users_xls, name="export_users_xls"),
    

    

    
    
    
    
]
